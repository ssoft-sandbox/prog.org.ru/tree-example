TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle

SOURCES *= \
    $$files( $${PWD}/src/*.cpp ) \
    $$files( $${PWD}/src/Hierarchy/*.cpp ) \
    $$files( $${PWD}/src/Graphics/*.cpp ) \

HEADERS *= \
    $$files( $${PWD}/src/*.h ) \
    $$files( $${PWD}/src/Hierarchy/*.h ) \
    $$files( $${PWD}/src/Hierarchy/*.hpp ) \
    $$files( $${PWD}/src/Graphics/*.h ) \

INCLUDEPATH *= $${PWD}/include
