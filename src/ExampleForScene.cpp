#include <Graphics/Scene.h>

#ifndef maybe_unused
#define maybe_unused void
#endif

template < typename _Type >
const _Type & as_const ( _Type & value ) { return value; }

template < typename _Type >
void as_const ( _Type && ) = delete;

void exampleForScene ()
{
    // Заполняем сцену

    Scene scene;
    scene.insertResource( "Hello", Resource() );
    scene.insertResource( "Camera", Resource() );
    scene.insertResource( "Texture", Resource() );
    scene.insertResource( "World", Resource() );
    scene.insertResource( "Model 1", Resource() );
    scene.insertResource( "Model 2", Resource() );
    scene.insertResource( "Model 3", Resource() );

    // Работа с деревом

    auto & tree = scene.transformTree();
        // создаем псевдоним для удобства

    // Последовательное заполнение

    auto root = *tree.append( scene.resource( "World" ) );
    (maybe_unused) root;
        // устанавливаем корневой элемент и создаем псевдоним
    auto m1 = *root.append( scene.resource( "Model 1" ) );
    (maybe_unused) m1;
        // псевдоним узла m1
    auto m2 = *m1.append( scene.resource( "Model 2 ") );
    (maybe_unused) m2;
        // псевдоним узла m2
    auto m3 = *root.append( scene.resource( "Model 3" ) );
    (maybe_unused) m3;
        // псевдоним узла m3
        // и т.д.

    m3.setValue( scene.resource( "Model 3.1" ) );

    // Проверка отношений

    assert( true == ::Hierarchy::areIdentical( m1, m1 ) );  // true
    assert( false == ::Hierarchy::areIdentical( m1, m2 ) ); // false
    assert( true == ::Hierarchy::areSibling( m1, m3 ) );    // true
    assert( false == ::Hierarchy::areSibling( m2, m3 ) );   // false
    assert( true == ::Hierarchy::isParentOf( m1, m2 ) );    // true
    assert( false == ::Hierarchy::isParentOf( m1, m3 ) );   // false
    assert( true == ::Hierarchy::isAncestorOf( m1, m2 ) );  // true
    assert( false == ::Hierarchy::isAncestorOf( m1, m3 ) ); // false


    assert( true == ::Hierarchy::isAncestorOf( m1, m2 ) );
        // проверяем, что узел m2 содержится в m1
    m3.transfer( m1 );
        // переносим все узлы из m1 в m3
    assert( true == ::Hierarchy::isAncestorOf( m3, m2 ) );
        // проверяем, что узел m2 теперь содержится в m3

    // Перебор по элементам дерева

    for ( auto node : /*as_const*/( tree ) )
        // начали с корней
    {
        (maybe_unused) node.value();
            // доступ к значению (ассоциация с ресурсом и любые дополнительные атрибуты)
        (maybe_unused) node.count();
            // получаем количество узлов следующего уровня вложенности

        for ( auto node1 : node )
            // пошли глубже
        {
            (maybe_unused) node1;
            // ...
        }
    }

    // Перебор по элементам дерева c применением лямбды

    using NodeReference = Scene::TransformTree::NodeWrapper;
    ::std::function< void( NodeReference ) > iterateChildren = [&] ( NodeReference item )
    {
        for ( NodeReference subnode : item )
            // пошли глубже
        {
            (maybe_unused) item.value();
                // доступ к значению (ассоциация с ресурсом и любые дополнительные атрибуты)
            (maybe_unused) item.count();
                // получаем количество узлов следующего уровня вложенности
            iterateChildren( subnode );
                // пошли глубже ...
        }
    };

    iterateChildren( root );
        // рекурсивный перебор элементов, начиная с root

    // Использование наблюдателя (дерева)

    // Произвольный доступ с помощью наблюдателя

    //auto & observer = tree.observer();
    //auto & observer = ::std::get<0>(tree.observer().tuple());
    auto & observer = tree.observer().get<0>();

    auto m1_item = observer.itemForResource( scene.resource( "Model 1" ) );
    (maybe_unused) *m1_item;
        // доступ к этому узлу
    (maybe_unused) (*m1_item).parent();
        // доступ к вышестоящему узлу
    auto before_item = m1_item;
    --before_item;
        // сосед до
    auto after_item = m1_item;
    ++after_item;
        // сосед после

    // Полный перебор в произвольном порядке с помощью наблюдателя

    for ( auto & res_and_item : observer.itemForResource() )
    {
        (maybe_unused) res_and_item.first;
            // ключ, по которому Observer индексирует узлы
        auto item = res_and_item.second;
            // информация об элементе и его родителе
        (maybe_unused) *item;
            // доступ к этому узлу
        (maybe_unused) (*item).parent();
            // доступ к вышестоящему узлу
        auto before_item = item;
        --before_item;
            // сосед до
        auto after_item = item;
        ++after_item;
            // сосед после
    }

    // Дополнительная информация с помощью наблюдателя

    size_t all_node_count = observer.nodeCount();
    (maybe_unused) all_node_count;
        // полное количество узлов в дереве

    scene.removeResource( "Model 3.1" );
    all_node_count = observer.nodeCount();

    // Копирование/извлечение/вставка

    TransformTree::Node raw_node_copied = root.access();
    (maybe_unused) raw_node_copied;
        // копирование базовых узлов из элементов дерева
    TransformTree::Node raw_node_moved = root.release( m1_item );
    (maybe_unused) raw_node_moved;
        // извлечение базовых узлов из элементов дерева
        // эквивалентно по результату: raw_node_moved = ::std::move( m1 );
    root.append( ::std::move( raw_node_moved ) );
        // вставка узлов обратно (перемещением)
}
