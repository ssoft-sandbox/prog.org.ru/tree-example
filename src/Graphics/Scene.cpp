#include <cassert>
#include <memory>
#include "Scene.h"

Scene::Scene()
    : m_transform_tree()
{
}

void Scene::insertResource ( const ResourceKey & key, Resource resource )
{
    m_resource[ key ] = ::std::move( resource );
}

Resource Scene::removeResource ( const ResourceKey & key )
{
    auto iter = m_resource.find( key );
    assert( iter != m_resource.end() );
        // Находим элемент

    //m_transform_tree.remove( m_transform_tree.observer().itemForResource( iter->second ) );
    //m_transform_tree.remove( ::std::get<0>( m_transform_tree.observer().tuple() ).itemForResource( iter->second ) );
    m_transform_tree.remove( m_transform_tree.observer().get<0>().itemForResource( iter->second ) );
        // Удаляем элементы дерева, связанные с данным ресурсом

    Resource result = ::std::move( iter->second );
    m_resource.erase( iter );
        // Удаляем элемент из места хранения ресурсов

    return result;
        // Возвращаем удаленный элемент
}

Resource & Scene::resource ( const ResourceKey & key )
{
    return m_resource[ key ];
}

const Resource & Scene::resource ( const ResourceKey & key ) const
{
    return m_resource.at( key );
}

Scene::TransformTree & Scene::transformTree ()
{
    return m_transform_tree;
}

const Scene::TransformTree & Scene::transformTree () const
{
    return m_transform_tree;
}
