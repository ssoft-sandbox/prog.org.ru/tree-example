#ifndef SCENE_H
#define SCENE_H

#include <string>
#include <unordered_map>
#include <functional>
#include <Graphics/Resource.h>
#include <Graphics/TransformTree.h>

class Scene
{
    // Сцена "владеет" ресурсами, которые образуют неупорядоченное множество,
    // индексированное по некоторому ключу. Вид ключа и способ хранения определяет
    // сама сцена. Ресурсы не имеют обратной связи со сценой. Управление ресурсами
    // осуществляется только через API сцены.

public:
    using ResourceKey = ::std::string;

    using Resource = ::Resource;
    using ResourceReference = Resource &;
    using ResourceConstReference = const Resource &;
    using Resources = ::std::unordered_map< ResourceKey, Resource >;
    using TransformTree = ::TransformTree;
    using TransformTreeReference = TransformTree &;
    using TransformTreeConstReference = const TransformTree &;

private:
    Resources m_resource;
        //!< место хранения ресурсов
    TransformTree m_transform_tree;
        //!< упорядочивание ресурсов в виде дерева для применения трансформаций

public:
    Scene ();
    void insertResource ( const ResourceKey & key, Resource resource );
        /*!<
         * Вставка ресурса в сцену. В качестве ресурса передается либо копия
         * значения типа Resource, либо используется семантика перемещения:
         * scene.insertResource( key, ::std::move( resource ) ).
         */
    Resource removeResource ( const ResourceKey & key );
        //!< Удаление ресурса из сцены по ключу.

    ResourceReference resource ( const ResourceKey & key );
        //!< Предоставляет доступ для изменения к ресурсам сцены по ключу key
    ResourceConstReference resource ( const ResourceKey & key ) const;
        //!< Предоставляет доступ для чтения к ресурсам сцены по ключу key

    TransformTreeReference transformTree ();
        //!< Предоставляет доступ для изменения к дереву применения трансформаций
    TransformTreeConstReference transformTree () const;
        //!< Предоставляет доступ для чтения к дереву применения трансформаций
};


#endif
