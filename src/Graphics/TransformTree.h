#ifndef TRANSFORM_TREE_H
#define TRANSFORM_TREE_H

#include <functional>
#include <memory>
#include <string>
#include <type_traits>
#include <cstddef>
#include <unordered_map>

#include <Graphics/Resource.h>
#include <Hierarchy/Tree.h>

struct TransformValue
    //!< Элемент дерева применения трансформаций
{
    using ResourceReference = ::std::reference_wrapper< Resource >;
    using Index = ::std::size_t;

    ResourceReference m_resource;
        //!< Aссоциация с ресурсом сцены.
        ///  (композитно агрегирована, неизменяема и не опциональна)

        // Далее при необходимости могут располагаться дополнительные атрибуты,
        // характеризующие данный вид взаимосвязи (например, действует или нет
        // на этот объект трансформация предыдущего в иерархии).

    Index m_index = Index();
        //!< Глобальный порядковый индекс взаимосвязи (для наглядности примера)

    TransformValue ( Resource & resource ) : m_resource( resource )
    {
        static Index counter = Index();
        m_index = counter++;
    }
};

namespace Graphics
{
    namespace Resource
    {
        template < typename _Tree >
        class TreeObserver
        {
            friend _Tree;

        public:
            using Tree = _Tree;

            using ResourceReference = ::Resource &;
            using ConstResourceReference = const ::Resource &;

            using Node = typename Tree::Node;
            using NodeCount = typename Node::NodeCount;

            using NodeItem = typename Tree::NodeWrapperIterator;
            using ConstNodeItem = typename Tree::ConstNodeWrapperIterator;
            using ConstValueReference = typename Tree::ConstValueReference;

            using ResourceAddress = ::Resource *;
            using ItemForResource = ::std::unordered_map< ResourceAddress, ConstNodeItem >;
            using ConstItemForResourceReference = const ItemForResource &;

        private:
            NodeCount m_node_count;
                // количество узлов в дереве
            ItemForResource m_item_for_address;
                // индексируем узлы по указателям на ресурсы

        public:
            constexpr TreeObserver () noexcept
                : m_node_count()
            {}

            template < typename ... _Arguments >
            TreeObserver ( _Arguments && ... arguments )
                : m_node_count()
            {
                emplaceRoot( ::std::forward< _Arguments >( arguments ) ... );
            }

            ConstNodeItem itemForResource ( ResourceReference ref ) const
            {
                auto index_iter = m_item_for_address.find( ::std::addressof( ref ) );
                assert( index_iter != m_item_for_address.end() );
                return index_iter->second;
            }

            ConstItemForResourceReference itemForResource () const
            {
                return m_item_for_address;
            }

            NodeCount nodeCount () const
            {
                return m_node_count;
            }

        //private: //< The friend _Tree declaration used
            void beforeClear ()
            {
                m_node_count = 0;
                m_item_for_address.clear();
            }

            void afterInsert ( NodeItem item )
            {
                ++m_node_count;
                ResourceAddress key = ::std::addressof( (*item).value().m_resource.get() );
                assert( !m_item_for_address.count( key ) );
                m_item_for_address.insert( { key, item } );
            }

            void beforeErase ( ConstNodeItem item )
            {
                --m_node_count;
                ResourceAddress key = ::std::addressof( (*item).value().m_resource.get() );
                assert( m_item_for_address.count( key ) );
                m_item_for_address.erase( key );
            }

            void onChange ( ConstValueReference current, ConstValueReference next )
            {
                ResourceAddress before_key = ::std::addressof( current.m_resource.get() );
                ResourceAddress after_key = ::std::addressof( next.m_resource.get() );
                if ( before_key != after_key )
                {
                    assert( m_item_for_address.count( before_key ) );
                    assert( !m_item_for_address.count( after_key ) );

                    auto handler = m_item_for_address.extract( before_key );
                    handler.key() = after_key;
                    m_item_for_address.insert( ::std::move( handler ) );
                }
            }
        };
    }
}

//using TransformTree = ::Hierarchy::Tree< TransformValue, ::Graphics::Resource::TreeObserver >;
template < typename _Tree >
using TestObserver = ::Hierarchy::TupleOfObservers< _Tree, ::Graphics::Resource::TreeObserver< _Tree > >;
using TransformTree = ::Hierarchy::Tree< TransformValue, TestObserver >;

#endif
