extern void exampleForParentChild ();
extern void exampleForScene ();

// next_permutation example
#include <iostream>     // std::cout
#include <algorithm>    // std::next_permutation, std::sort


int main ( int, char ** )
{
    exampleForParentChild();
    exampleForScene();
    return 0;
}

