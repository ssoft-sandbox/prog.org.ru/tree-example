#pragma once
#ifndef HIERARCHY_NODE_H
#define HIERARCHY_NODE_H

#include <cassert>
#include <Hierarchy/Algorithm.h>
#include <list>
#include <memory>
#include <type_traits>

namespace Hierarchy
{
    template <
        typename _Value,
        template < typename > class _Allocator = ::std::allocator >
    class Node
        /*!<
         * Узел дерева, реализующий отношение parent-child.
         * На основе данного отношения можно реализовать деревья различного типа.
         */
    {
        using ThisType = Node< _Value, _Allocator >;
        using Allocator = _Allocator< ThisType >;

    public:
        using Value = _Value;
        using ValueReference = Value &;
        using ConstValueReference = const Value &;

        using NodeReference = ThisType &;
        using ConstNodeReference = const ThisType &;

        using Nodes = ::std::list< ThisType, Allocator >;
        using Iterator = typename Nodes::iterator;
        using ConstIterator = typename Nodes::const_iterator;

        using NodeCount = typename Nodes::size_type;
        /*
         * Для хранения узлов используется ::std::list, так как любые
         * операции вставки/удаления не "портят" его итераторы.
         *
         * C++ Standard.
         * Iterator validity:
         *   insert - No changes.
         *   erase - Iterators, pointers and references referring to elements removed by the function are invalidated.
         *           All other iterators, pointers and references keep their validity.
         */

        using NodeOptionalReference = NodeReference; // TODO: что-то типа ::std::optional< ::std::reference_wrapper< ThisType > >;
        using ConstNodeOptionalReference = const NodeOptionalReference;

    public:
        using ParentOptionalAssociation = ThisType *;

    private:
        //mutable NodeOptionalReference m_parent;     //!< Ассоциация с родительским узлом, [0..1].
        mutable ParentOptionalAssociation m_parent; //!< Ассоциация с родительским узлом, [0..1].
        Nodes m_nodes;                              //!< Композитно агрегированные дочерние узлы, [0..N].
        Value m_value;                              //!< Композитно агрегированное значение, [1..1].

    public:
        Node ( ThisType && other );
        Node ( const ThisType && other ) : Node( static_cast< const ThisType & >( other ) ) {}
        Node ( ThisType & other ) : Node( const_cast< const ThisType & >( other ) ) {}
        Node ( const ThisType & other );
            // Требуется реализация всех констукторов по умолчанию из-за наличия
            // variadic template конструктора Node( _Arguments && ... arguments ).

        template < typename ... _Arguments >
        Node ( _Arguments && ... arguments )
            : m_parent()
            , m_value( ::std::forward< _Arguments >( arguments ) ... )
        {}

        void operator = ( const ThisType & ) = delete;
        void operator = ( ThisType && ) = delete;
            // запрет на операции равенства и перемещения между узлами

        ValueReference value () noexcept { return m_value; }
            //!< Предоставляет доступ на запись к значению узла
        ConstValueReference value () const noexcept { return m_value; }
            //!< Предоставляет доступ на чтение к значению узла

        NodeOptionalReference parent () noexcept { return *m_parent; }
        ConstNodeOptionalReference parent () const noexcept { return *m_parent; }

        bool isEmpty () const noexcept { return m_nodes.empty(); }
        NodeCount count () const noexcept { return m_nodes.size(); }

        NodeReference first () noexcept { return m_nodes.front(); }
        ConstNodeReference first () const noexcept { return m_nodes.front(); }
        NodeReference last () noexcept { return m_nodes.back(); }
        ConstNodeReference last () const noexcept { return m_nodes.back(); }

        template < typename ... _Arguments >
        Iterator append ( _Arguments && ... arguments )
            { return emplace( end(), ::std::forward< _Arguments >( arguments ) ... ); }
        template < typename ... _Arguments >
        Iterator prepend ( _Arguments && ... arguments )
            { return emplace( begin(), ::std::forward< _Arguments >( arguments ) ... ); }
        template < typename ... _Arguments >
        Iterator emplace ( ConstIterator before_position, _Arguments && ... arguments );

        template < typename _Iterator >
        _Iterator erase ( _Iterator position ) noexcept;
        template < typename _Iterator >
        _Iterator erase ( _Iterator first, _Iterator last ) noexcept;
            // NOTE: Стандартом определено поведение, когда возвращаемый
            // тип является iterator, а входящий const_iterator.
            // В этом случае имеется "уязвимость" в виде возможности неявного
            // преобразование const_iterator к iterator.
            // iterator iter = erase( const_iter, const_iter ); // constless HACK

        void clear () noexcept { m_nodes.clear(); }

        Node take ( ConstIterator position ) noexcept;
        Node takeFirst () noexcept;
        Node takeLast () noexcept;

        void transfer ( NodeReference from_node ) noexcept { return transfer( cend(), from_node ); }
        void transfer ( NodeReference from_node, ConstIterator from_position ) noexcept { return transfer( cend(), from_node, from_position ); }
        void transfer ( NodeReference from_node, ConstIterator from_first, ConstIterator from_last ) noexcept { return transfer( cend(), from_node, from_first, from_last ); }
        void transfer ( ConstIterator before_position, NodeReference from_node ) noexcept;
        void transfer ( ConstIterator before_position, NodeReference from_node, ConstIterator from_position ) noexcept;
        void transfer ( ConstIterator before_position, NodeReference from_node, ConstIterator from_first, ConstIterator from_last ) noexcept;

        Iterator begin () noexcept { return m_nodes.begin(); }
        Iterator end () noexcept { return m_nodes.end(); }

        ConstIterator begin () const noexcept { return m_nodes.begin(); }
        ConstIterator end () const noexcept { return m_nodes.end(); }

        ConstIterator cbegin () const noexcept { return m_nodes.cbegin(); }
        ConstIterator cend () const noexcept { return m_nodes.cend(); }

    public:
        // std concepts compatibility
        using value_type = typename Nodes::value_type;
        using pointer = typename Nodes::pointer;
        using const_pointer = typename Nodes::const_pointer;
        using reference = typename Nodes::reference;
        using const_reference = typename Nodes::const_reference;
        using iterator = typename Nodes::iterator;
        using const_iterator = typename Nodes::const_iterator;
        using reverse_iterator = typename Nodes::reverse_iterator;
        using const_reverse_iterator = typename Nodes::const_reverse_iterator;
        using size_type = typename Nodes::size_type;
        using difference_type = typename Nodes::difference_type;
        using allocator_type = typename Nodes::allocator_type;

        reference front () noexcept { return first(); }
        const_reference front () const noexcept { return first(); }
        reference back () noexcept { return last(); }
        const_reference back () const noexcept { return last(); }

        bool empty () const noexcept { return isEmpty(); }
        NodeCount size () const noexcept { return count(); }
    };
}

#endif
