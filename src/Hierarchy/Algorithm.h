#pragma once
#ifndef HIERARCHY_ALGORITHM_H
#define HIERARCHY_ALGORITHM_H

#include <memory>

namespace Hierarchy
{
    template < typename _Value >
    struct ParentOf
    {
        static constexpr decltype(auto) get ( _Value && value )
        {
            return ::std::forward< _Value >( value ).parent();
        }
    };
}

namespace Hierarchy
{
    template < typename _FirstValue, typename _SecondValue >
    struct IdentityOf
    {
        static constexpr bool check ( _FirstValue && first, _SecondValue && second )
        {
            return first == second;
        }
    };

    template < typename _FirstValue, typename _SecondValue >
    struct IdentityOf< _FirstValue &, _SecondValue & >
    {
        static constexpr bool check ( _FirstValue & first, _SecondValue & second )
        {
            return ::std::addressof( first ) == ::std::addressof( second )
                && ::std::addressof( first ) != nullptr;
        }
    };
}

namespace Hierarchy
{
    template < typename _Value >
    inline constexpr decltype(auto) parentOf ( _Value && value )
    {
        return ParentOf< _Value >::get( ::std::forward< _Value >( value ) );
    }

    template < typename _FirstValue, typename _SecondValue >
    inline constexpr bool areIdentical ( _FirstValue && first, _SecondValue && second )
    {
        return IdentityOf< _FirstValue, _SecondValue >::check(
            ::std::forward< _FirstValue >( first ), ::std::forward< _SecondValue >( second ) );
    }

    template < typename _Value >
    inline constexpr bool hasParent ( _Value && value )
    {
        return areIdentical( parentOf( ::std::forward< _Value >( value ) )
            , parentOf( ::std::forward< _Value >( value ) ) );
    }

    template < typename _Parent, typename _Value >
    inline constexpr bool isParentOf ( _Parent && parent, _Value && value )
    {
        return ::Hierarchy::areIdentical( ::std::forward< _Parent >( parent )
            , parentOf( ::std::forward< _Value >( value ) ) );
    }

    template < typename _Parent, typename _Value >
    inline constexpr bool isAncestorOf ( _Parent && parent, _Value && value )
    {
        if ( !::Hierarchy::hasParent( ::std::forward< _Value >( value ) ) )
            return false;

        if ( ::Hierarchy::isParentOf( ::std::forward< _Parent >( parent ), ::std::forward< _Value >( value ) ) )
            return true;

        return ::Hierarchy::isAncestorOf( ::std::forward< _Parent >( parent ), parentOf( ::std::forward< _Value >( value ) ) );
    }

    template < typename _ParentedFirst, typename _ParentedSecond >
    inline constexpr bool areSibling ( _ParentedFirst && first, _ParentedSecond && second )
    {
        return ::Hierarchy::areIdentical( parentOf( ::std::forward< _ParentedFirst >( first ) )
            , parentOf( ::std::forward< _ParentedSecond >( second ) ) );
    }
}

#endif
