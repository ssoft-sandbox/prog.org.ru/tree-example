#pragma once
#ifndef HIERARCHY_TREE_H
#define HIERARCHY_TREE_H

#include <Hierarchy/Node.h>
#include <tuple>

namespace Hierarchy
{
    template <
        typename _Value,
        template < typename > class _Observer,
        template < typename > class _Allocator = ::std::allocator >
    class Tree
    {
        using ThisType = Tree< _Value, _Observer, _Allocator >;

    public:
        using Value = _Value;
        using ConstValue = const Value;
        using ValueReference = Value &;
        using ConstValueReference = const Value &;

        using Observer = _Observer< ThisType >;
        using ObserverReference = Observer &;
        using ConstObserverReference = const Observer &;

        using Node = ::Hierarchy::Node< Value, _Allocator >;
        using Nodes = typename Node::Nodes;

    public:
        class NodeWrapper;
        class ConstNodeWrapper;

        class NodeWrapperIterator;
        class ConstNodeWrapperIterator;

    private:
        template < typename _Type >
        class ArrowOperator
        {
            _Type m_value;

        public:
            template < typename ... _Arguments >
            ArrowOperator ( _Arguments ... arguments )
                : m_value( ::std::forward< _Arguments >( arguments ) ... )
            {}

            auto operator -> () && { return ::std::addressof( m_value ); }
        };


    public:
        class NodeWrapperIterator
        {
            using ThisType = NodeWrapperIterator;

            template <
                typename,
                template < typename > class,
                template < typename > class >
            friend class Tree;
            friend class ConstNodeIterator;

        public:
            using Tree = ::Hierarchy::Tree< _Value, _Observer, _Allocator >;
            using TreeReference = Tree &;
            using ConstTreeReference = const Tree &;

            using NodeWrapper = typename Tree::NodeWrapper;
            using ConstNodeWrapper = typename Tree::ConstNodeWrapper;

        private:
            using NodeIterator = typename Node::Iterator;
            using TreeAddress = Tree *;

        private:
            TreeAddress m_tree_address = TreeAddress(); //< опциональная ассоциация с деревом
            NodeIterator m_node_iter;                   //< подвижная ассоциация с узлом контейнера

        private:
            NodeWrapperIterator ( TreeAddress tree, NodeIterator iter ) noexcept
                : m_tree_address( tree ), m_node_iter( iter ) {}

        public:
            NodeWrapperIterator () = default;

            bool operator == ( const ThisType & other ) const noexcept { return m_node_iter == other.m_node_iter; }
            bool operator != ( const ThisType & other ) const noexcept { return !( *this == other ); }

            ThisType & operator ++ () { ++m_node_iter; return *this; }
            ThisType & operator -- () { --m_node_iter; return *this; }

            ThisType operator ++ ( int ) { return { m_tree_address, m_node_iter++ }; }
            ThisType operator -- ( int ) { return { m_tree_address, m_node_iter-- }; }

            NodeWrapper operator * () { return { *m_tree_address, *m_node_iter }; }
            ConstNodeWrapper operator * () const { return { m_tree_address, *m_node_iter }; }

            ArrowOperator< NodeWrapper > operator -> () { return { *m_tree_address, *m_node_iter }; }
            ArrowOperator< ConstNodeWrapper > operator -> () const { return { *m_tree_address, *m_node_iter }; }
        };

    public:
        class ConstNodeWrapperIterator
        {
            using ThisType = ConstNodeWrapperIterator;

            template <
                typename,
                template < typename > class,
                template < typename > class >
            friend class Tree;

        public:
            using Tree = ::Hierarchy::Tree< _Value, _Observer, _Allocator >;
            using TreeReference = Tree &;
            using ConstTreeReference = const Tree &;

            using Wrapper = typename Tree::NodeWrapper;
            using ConstNodeWrapper = typename Tree::ConstNodeWrapper;

        private:
            using ConstNodeIterator = typename Node::ConstIterator;
            using TreeConstAddress = Tree const *;

        private:
            TreeConstAddress m_tree_address = TreeConstAddress();   //< опциональная ассоциация с деревом
            ConstNodeIterator m_node_iter;                          //< подвижная ассоциация с узлом контейнера

        private:
            ConstNodeWrapperIterator ( TreeConstAddress tree, ConstNodeIterator iter ) noexcept
                : m_tree_address( tree ), m_node_iter( iter ) {}

        public:
            ConstNodeWrapperIterator () = default;
            ConstNodeWrapperIterator ( ThisType && ) noexcept = default;
            ConstNodeWrapperIterator ( ThisType & ) noexcept = default;
            ConstNodeWrapperIterator ( const ThisType & ) noexcept = default;

            ConstNodeWrapperIterator ( const NodeWrapperIterator & other ) noexcept
                : m_tree_address( other.m_tree_address ), m_node_iter( other.m_node_iter ) {}

            ThisType & operator = ( const ThisType & other ) noexcept = default;
            ThisType & operator = ( const NodeWrapperIterator & other ) noexcept
                { m_tree_address = other.m_tree_address; m_node_iter = other.m_node_iter; return *this; }

            bool operator == ( const ThisType & other ) const noexcept { return  m_node_iter == other.m_node_iter; }
            bool operator != ( const ThisType & other ) const noexcept { return  !( *this == other ); }

            ThisType & operator ++ () { ++m_node_iter; return *this; }
            ThisType & operator -- () { --m_node_iter; return *this; }

            ThisType operator ++ ( int ) { return { *m_tree_address, m_node_iter++ }; }
            ThisType operator -- ( int ) { return { *m_tree_address, m_node_iter-- }; }

            ConstNodeWrapper operator * () const { return { *m_tree_address, *m_node_iter }; }
            ArrowOperator< ConstNodeWrapper > operator -> () const { return { *m_tree_address, *m_node_iter }; }
        };

    public:
        class NodeWrapper
        {
            using ThisType = NodeWrapper;

            template <
                typename,
                template < typename > class,
                template < typename > class >
            friend class Tree;
            friend class ConstNodeWrapper;

        public:
            using Tree = ::Hierarchy::Tree< _Value, _Observer, _Allocator >;
            using TreeReference = Tree &;
            using ConstTreeReference = const Tree &;

            using NodeReference = Node &;
            using ConstNodeReference = const Node &;

            using ValueReference = Value &;
            using ConstValueReference = const Value &;

            using NodeWrapperIterator = typename Tree::NodeWrapperIterator;
            using ConstNodeWrapperIterator = typename Tree::ConstNodeWrapperIterator;

            using NodeCount = typename Node::NodeCount;

        private:
            TreeReference m_tree_ref;     //< неизменяемая ассоциация с деревом
            NodeReference m_node_ref;     //< неизменяемая ассоциация с узлом

        private:
            NodeWrapper ( TreeReference tree, NodeReference node ) noexcept
                : m_tree_ref( tree ), m_node_ref( node ) {}

        public:
            TreeReference tree () noexcept { return m_tree_ref; }
            ConstTreeReference tree () const noexcept { return m_tree_ref; }

            void setValue ( ConstValueReference value )
            {
                m_tree_ref.notifyOnChange( m_node_ref.value(), value );
                m_node_ref.value() = value;
            }
            ConstValueReference value () const { return m_node_ref.value(); }

            //ValueWrapper value () { return m_node_ref.value(); }
            //ConstValueWrapper value () const { return m_node_ref.value(); }

            // Далее следует повтор интерфейса  ParentChild::Node, так как
            // данный вид дерева подразумевает полный повтор функциональности Node.
            // Для других видов деревьев это не актуально.

            NodeWrapper parent () noexcept { return { m_tree_ref, m_node_ref.parent() }; }
            ConstNodeWrapper parent () const noexcept { return { m_tree_ref, m_node_ref.parent() }; }

            bool operator == ( ConstNodeWrapper other ) const noexcept { return ::Hierarchy::areIdentical( m_node_ref,  other.m_node_ref ); }
            bool operator != ( ConstNodeWrapper other ) const noexcept { return !( *this == other ); }

            bool isEmpty () const noexcept { return m_node_ref.isEmpty(); }
            NodeCount count () const { return m_node_ref.count(); }

            NodeWrapper first () noexcept { return { m_tree_ref, m_node_ref.first() }; }
            ConstNodeWrapper first () const noexcept { return { m_tree_ref, m_node_ref.first() }; }
            NodeWrapper last () noexcept { return { m_tree_ref, m_node_ref.last() }; }
            ConstNodeWrapper last () const noexcept { return { m_tree_ref, m_node_ref.last() }; }

            template < typename ... _Arguments >
            NodeWrapperIterator append ( _Arguments && ... arguments ) { return emplace( cend(), ::std::forward< _Arguments >( arguments ) ... ); }
            template < typename ... _Arguments >
            NodeWrapperIterator prepend ( _Arguments && ... arguments ) { return emplace( cbegin(), ::std::forward< _Arguments >( arguments ) ... ); }

            template < typename ... _Arguments >
            NodeWrapperIterator emplace ( ConstNodeWrapperIterator before_position, _Arguments && ... arguments )
            {
                NodeWrapperIterator item{ ::std::addressof( m_tree_ref ), m_node_ref.emplace( before_position.m_node_iter, ::std::forward< _Arguments >( arguments ) ... ) };
                m_tree_ref.notifyAfterInsert( item );
                return item;
            }

            template < typename _Item >
            _Item erase ( _Item position ) noexcept
            {
                m_tree_ref.notifyBeforeErase( position );
                return { ::std::addressof( m_tree_ref ), m_node_ref.erase( position.m_node_iter ) };
            }

            template < typename _Item >
            _Item erase ( _Item first, _Item last ) noexcept
            {
                for ( auto item = first; item != last; ++item )
                    m_tree_ref.notifyBeforeErase( item );
                return { ::std::addressof( m_tree_ref ), m_node_ref.erase( first.m_node_iter, last.m_node_iter ) };
            }

            void clear () noexcept
            {
                for ( auto item = cbegin(); item != cend(); ++item )
                    m_tree_ref.notifyBeforeErase( item );
                m_node_ref.clear();
            }

            Node release ( ConstNodeWrapperIterator position ) noexcept
            {
                m_tree_ref.notifyBeforeErase( position );
                return m_node_ref.take( position.m_node_iter );
            }

            ConstNodeReference access () const noexcept { return m_node_ref; }

            void transfer ( NodeWrapper from_node ) noexcept
                { transfer( cend(), from_node ); }

            void transfer ( ConstNodeWrapperIterator before_position, NodeWrapper from_node ) noexcept
            {
                bool need_to_notify = ::std::addressof( m_tree_ref ) != ::std::addressof( from_node.m_tree_ref );
                    // Если перенос в другое дерево, то нужно оповещать наблюдателей

                if ( need_to_notify )
                    for ( ConstNodeWrapperIterator item = from_node.cbegin(); item != from_node.cend(); ++item )
                        from_node.m_tree_ref.notifyBeforeErase( item );

                NodeCount transfer_count = from_node.count();
                NodeWrapperIterator to_position{ ::std::addressof( m_tree_ref ), from_node.begin().m_node_iter };
                    // определяем откуда и сколько итерировать после переноса afterErase.

                m_node_ref.transfer( before_position.m_node_iter, from_node.m_node_ref );

                if ( need_to_notify )
                    for ( NodeCount i = NodeCount(); i < transfer_count; ++i )
                        m_tree_ref.notifyAfterInsert( to_position++ );
            }

            void transfer ( NodeWrapper from_node, ConstNodeWrapperIterator from_position ) noexcept
                { transfer( cend(), from_node, from_position ); }

            void transfer ( ConstNodeWrapperIterator before_position, NodeWrapper from_node, ConstNodeWrapperIterator from_position ) noexcept
            {
                bool need_to_notify = ::std::addressof( m_tree_ref ) != ::std::addressof( from_node.m_tree_ref );
                    // Если перенос в другое дерево, то нужно оповещать наблюдателей

                if ( need_to_notify )
                    from_node.m_tree_ref.notifyBeforeErase( from_position );

                ConstNodeWrapperIterator to_position{ ::std::addressof( m_tree_ref ), from_position.m_node_iter };
                m_node_ref.transfer( before_position.m_node_iter, from_node.m_node_ref, from_position.m_node_iter );

                if ( need_to_notify )
                        m_tree_ref.notifyAfterInsert( to_position );
            }

            void transfer ( NodeWrapper from_node, ConstNodeWrapperIterator from_first, ConstNodeWrapperIterator from_last ) noexcept
                { transfer( cend(), from_node, from_first, from_last ); }

            void transfer ( ConstNodeWrapperIterator before_position, NodeWrapper from_node, ConstNodeWrapperIterator from_first, ConstNodeWrapperIterator from_last ) noexcept
            {
                bool need_to_notify = ::std::addressof( m_tree_ref ) != ::std::addressof( from_node.m_tree_ref );
                    // Если перенос в другое дерево, то нужно оповещать наблюдателей

                NodeCount transfer_count = NodeCount();

                if ( need_to_notify )
                    for ( ConstNodeWrapperIterator item = from_first; item != from_last; ++item, ++transfer_count )
                        from_node.m_tree_ref.notifyBeforeErase( item );

                ConstNodeWrapperIterator to_position{ ::std::addressof( m_tree_ref ), from_first.m_node_iter };
                    // определяем откуда и сколько итерировать после переноса afterErase.

                m_node_ref.transfer( before_position.m_node_iter, from_node.m_node_ref
                    , from_first.m_node_iter, from_last.m_node_iter );

                if ( need_to_notify )
                    for ( NodeCount i = NodeCount(); i < transfer_count; ++i )
                        m_tree_ref.notifyAfterInsert( to_position++ );
            }

            NodeWrapperIterator begin () noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.begin() }; }
            ConstNodeWrapperIterator begin () const noexcept { return cbegin(); }
            ConstNodeWrapperIterator cbegin () const noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.cbegin() }; }

            NodeWrapperIterator end () noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.end() }; }
            ConstNodeWrapperIterator end () const noexcept { return cend(); }
            ConstNodeWrapperIterator cend () const noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.cend() }; }
        };

    public:
        class ConstNodeWrapper
        {
            using ThisType = ConstNodeWrapper;

            template <
                typename,
                template < typename > class,
                template < typename > class >
            friend class Tree;
            friend class NodeWrapper;

        public:
            using Tree = ::Hierarchy::Tree< _Value, _Observer, _Allocator >;
            using TreeReference = Tree &;
            using ConstTreeReference = const Tree &;

            using NodeReference = Node &;
            using ConstNodeReference = const Node &;

            using ValueReference = const Value &;
            using ConstValueReference = const Value &;
                // TODO: реализовать обертку ValueWrapper
                // для возможности реакции Tree на изменение значения
                // без явного применения метода setValue.

            using NodeWrapperIterator = typename Tree::NodeWrapperIterator;
            using ConstNodeWrapperIterator = typename Tree::ConstNodeWrapperIterator;

            using NodeCount = typename Node::NodeCount;

        private:
            ConstTreeReference m_tree_ref;     //< неизменяемая ассоциация с деревом
            ConstNodeReference m_node_ref;     //< неизменяемая ассоциация с узлом

        private:
            ConstNodeWrapper ( ConstTreeReference tree, ConstNodeReference node ) noexcept
                : m_tree_ref( tree ), m_node_ref( node ) {}

        public:
            ConstNodeWrapper ( ThisType && ) noexcept = default;
            ConstNodeWrapper ( ThisType & ) noexcept = default;
            ConstNodeWrapper ( const ThisType & ) noexcept = default;

            ConstNodeWrapper ( NodeWrapper other )
                : m_tree_ref( other.m_tree_ref ), m_node_ref( other.m_node_ref ) {}

            ConstTreeReference tree () const noexcept { return m_tree_ref; }
            ConstValueReference value () const noexcept { return m_node_ref.value(); }

            ConstNodeWrapper parent () const noexcept { return { m_tree_ref, m_node_ref.parent() }; }
            ConstNodeReference access () const noexcept { return m_node_ref; }

            bool operator == ( ConstNodeWrapper other ) const noexcept { return ::Hierarchy::areIdentical( m_node_ref,  other.m_node_ref ); }
            bool operator != ( ConstNodeWrapper other ) const noexcept { return !( *this == other ); }

            bool isEmpty () const noexcept { return m_node_ref.isEmpty(); }
            NodeCount count () const { return m_node_ref.count(); }

            ConstNodeWrapper first () const noexcept { return { m_tree_ref, m_node_ref.first() }; }
            ConstNodeWrapper last () const noexcept { return { m_tree_ref, m_node_ref.last() }; }

            ConstNodeWrapperIterator begin () const noexcept { return cbegin(); }
            ConstNodeWrapperIterator cbegin () const noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.cbegin() }; }

            ConstNodeWrapperIterator end () const noexcept { return cend(); }
            ConstNodeWrapperIterator cend () const noexcept { return { ::std::addressof( m_tree_ref ), m_node_ref.cend() }; }
        };

    private:
        Observer m_observer;    //!< Обозреватель
        Nodes m_roots;          //!< Корни дерева

    public:
        ObserverReference observer () noexcept { return m_observer; }
        ConstObserverReference observer () const noexcept { return m_observer; }

        bool isEmpty () const noexcept { return m_roots.empty(); }

        template < typename ... _Arguments >
        NodeWrapperIterator append ( _Arguments && ... arguments )
            { return emplace( cend(), ::std::forward< _Arguments >( arguments ) ... ); }
        template < typename ... _Arguments >
        NodeWrapperIterator prepend ( _Arguments && ... arguments )
            { return emplace( cbegin(), ::std::forward< _Arguments >( arguments ) ... ); }
        template < typename ... _Arguments >
        NodeWrapperIterator emplace ( ConstNodeWrapperIterator before_position, _Arguments && ... arguments )
        {
            NodeWrapperIterator iter{ this, m_roots.emplace( before_position.m_node_iter, ::std::forward< _Arguments >( arguments ) ... ) };
            notifyAfterInsert( iter );
            return iter;
        }

        template < typename _Iterator >
        _Iterator erase ( _Iterator position ) noexcept
        {
            assert( !::Hierarchy::hasParent( *position ) );
                // It must be a root iter.
            notifyBeforeErase( position );
            return { this, m_roots.erase( position.m_node_iter ) };
        }

        template < typename _Iterator >
        _Iterator erase ( _Iterator first, _Iterator && last ) noexcept
        {
            assert( !::Hierarchy::hasParent( *first ) && !::Hierarchy::hasParent( *last ) );
                // It must be a root iter.
            for ( auto iter = first; iter != last; ++iter )
                (*iter).beforeErase( iter );
            return { this, m_roots.erase( first.m_node_iter, last.m_node_iter ) };
        }

        void clear () noexcept
        {
            notifyBeforeClear();
            m_roots.clear();
        }

        template < typename _Iterator >
        void remove ( _Iterator position )
        {
            assert( this == position.m_tree_address );
            NodeWrapper node_wrapper{ *this, const_cast< Node & >( *position.m_node_iter ) };
            if ( ::Hierarchy::hasParent( node_wrapper ) )
                node_wrapper.parent().erase( position );
            else
                erase( position );
        }

        NodeWrapperIterator begin () noexcept { return { this, m_roots.begin() }; }
        ConstNodeWrapperIterator begin () const noexcept { return cbegin(); }
        ConstNodeWrapperIterator cbegin () const noexcept { return { this, m_roots.cbegin() }; }

        NodeWrapperIterator end () noexcept { return { this, m_roots.end() }; }
        ConstNodeWrapperIterator end () const noexcept { return cend(); }
        ConstNodeWrapperIterator cend () const noexcept { return { this, m_roots.cend() }; }

    private:
        // Using observer contract methods
        void notifyAfterInsert ( NodeWrapperIterator iter )
        {
            m_observer.afterInsert( iter );
            NodeWrapper node = (*iter);
            for ( auto subiter = node.begin();
                subiter != node.end(); ++subiter )
                    notifyAfterInsert( subiter );
        }

        void notifyBeforeErase ( ConstNodeWrapperIterator iter )
        {
            ConstNodeWrapper node = (*iter);
            for ( auto subiter = node.begin();
                subiter != node.end(); ++subiter )
                    notifyBeforeErase( subiter );
            m_observer.beforeErase( iter );
        }

        void notifyBeforeClear ()
        {
            m_observer.beforeClear();
        }

        void notifyOnChange ( ConstValueReference current, ConstValueReference next )
        {
            m_observer.onChange( current, next );
        }
    };
}

namespace Hierarchy
{
    template < typename _Tree, typename ... _Observers >
    class TupleOfObservers
    {
        friend _Tree;

    public:
        using Tree = _Tree;

        using NodeItem = typename Tree::NodeWrapperIterator;
        using ConstNodeItem = typename Tree::ConstNodeWrapperIterator;
        using ConstValueReference = typename Tree::ConstValueReference;

        using Observers = ::std::tuple< _Observers ... >;
        using ObserversReference = Observers &;
        using ConstObserversReference = const Observers &;

    private:
        Observers m_observers;

    public:
        constexpr ObserversReference tuple () noexcept { return m_observers; }
        constexpr ConstObserversReference tuple () const noexcept { return m_observers; }

        template < ::std::size_t _I >
        constexpr decltype( auto ) get () noexcept { return ::std::get< _I >( m_observers ); }
        template < ::std::size_t _I >
        constexpr decltype( auto ) get () const noexcept { return ::std::get< _I >( m_observers ); }

        template < typename _Type >
        constexpr decltype( auto ) get () noexcept { return ::std::get< _Type >( m_observers ); }
        template < typename _Type >
        constexpr decltype( auto ) get () const noexcept { return ::std::get< _Type >( m_observers ); }

    private:
        constexpr void beforeClear ()
        {
            ::std::apply( []( auto && ... observers ) { ( (observers.beforeClear()), ... ); }, m_observers );
        }

        constexpr void afterInsert ( NodeItem item )
        {
            ::std::apply( [&]( auto && ... observers ) { ( (observers.afterInsert( item )), ... ); }, m_observers );
        }

        constexpr void beforeErase ( ConstNodeItem item )
        {
            ::std::apply( [&]( auto && ... observers ) { ( (observers.beforeErase( item )), ... ); }, m_observers );
        }

        constexpr void onChange ( ConstValueReference old_value, ConstValueReference new_value )
        {
            ::std::apply( [&]( auto && ... observers ) { ( (observers.onChange( old_value, new_value )), ... ); }, m_observers );
        }
    };
}

#endif
