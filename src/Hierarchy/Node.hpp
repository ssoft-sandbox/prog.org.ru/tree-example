#pragma once
#ifndef HIERARCHY_NODE_HPP
#define HIERARCHY_NODE_HPP

#include "Node.h"

namespace Hierarchy
{
    template <
        typename _Value,
        template < typename > class _Allocator >
    Node< _Value, _Allocator >::Node ( const ThisType & other )
        : m_parent()
        , m_nodes()
        , m_value( other.m_value )
    {
        for ( ConstNodeReference child : other )
            append( child );
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    Node< _Value, _Allocator >::Node ( ThisType && other )
        : m_parent()
        , m_nodes( ::std::forward< Nodes >( other.m_nodes ) )
        , m_value( ::std::forward< Value >( other.m_value ) )
    {
        if ( other.m_parent )
            // Извлекаем узел из родителя (линейный алгоритм)
        {
            ThisType & parent = *other.m_parent;
            for ( auto iter = parent.cbegin(); iter != parent.cend(); ++iter )
                // Перебираем у родителя other последовательно все элементы
            {
                if ( ::Hierarchy::areIdentical( *this, *iter ) )
                    // пока не встретим other
                {
                    erase( iter );
                    break;
                }
            }
        }

        for ( ThisType & node : m_nodes )
            node.m_parent = this;
            // всем дочерним узлам other задаем в качестве родителя this
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    template < typename ... _Arguments >
    inline
    typename Node< _Value, _Allocator >::Iterator
        Node< _Value, _Allocator >::emplace ( ConstIterator before_position, _Arguments && ... arguments )
    {
        assert( before_position == end() || ::Hierarchy::isParentOf( *this, *before_position ) );
            //< позиция before_position должна принадлежать узлу this или быть end.

        Iterator result = m_nodes.emplace( before_position, ::std::forward< _Arguments >( arguments ) ... );
        result->m_parent = this;
        return result;
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    template < typename _Iterator >
    inline
    _Iterator Node< _Value, _Allocator >::erase ( _Iterator position ) noexcept
    {
        assert( position != end() && ::Hierarchy::isParentOf( *this, *position ) );
            //< позиция position должна принадлежать узлу this и не быть end
        return { m_nodes.erase( position ) };
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    template < typename _Iterator >
    inline
    _Iterator Node< _Value, _Allocator >::erase ( _Iterator first, _Iterator last ) noexcept
    {
        assert( first != end() && ::Hierarchy::isParentOf( *first ) );
            //< позиция first должна принадлежать узлу this и не быть end
        assert( last == end() || ::Hierarchy::isParentOf( *last ) );
            //< позиция last должна принадлежать узлу this или быть end

        return m_nodes.erase( first, last );
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    inline
    Node< _Value, _Allocator > Node< _Value, _Allocator >::take ( ConstIterator position ) noexcept
    {
        assert( position != cend() && ::Hierarchy::isParentOf( *this, *position ) );
            //< позиция position должна принадлежать узлу this и не быть end
        ConstNodeReference child = *position;
        child.m_parent = ParentOptionalAssociation();
            //< чтобы перенос не затронул этот узел
        Node result = ::std::move( child );
            //< перенос содержимого узла в result
        m_nodes.erase( position );
            //< удаление теперь уже пустого узла
        return result;
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    inline
    Node< _Value, _Allocator > Node< _Value, _Allocator >::takeFirst () noexcept
    {
        return take( cbegin() );
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    inline
    Node< _Value, _Allocator > Node< _Value, _Allocator >::takeLast () noexcept
    {
        return take( --cend() );
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    void Node< _Value, _Allocator >::transfer ( ConstIterator before_position, NodeReference from_node ) noexcept
    {
        assert( before_position == cend() || ::Hierarchy::isParentOf( *this, *before_position ) );
            //< позиция before_position должна принадлежать узлу this или быть end
        assert( !::Hierarchy::areIdentical( from_node, *this ) );
            //< from_node не должен быть this
        assert( !::Hierarchy::isAncestorOf( from_node, *this ) );
            //< from_node не должен содержать this

        for ( NodeReference child : from_node )
            child.m_parent = this;

        m_nodes.splice( before_position, from_node.m_nodes );
            // Complexity - Constant.
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    inline
    void Node< _Value, _Allocator >::transfer ( ConstIterator before_position, NodeReference from_node, ConstIterator from_position ) noexcept
    {
        assert( before_position == cend() || ::Hierarchy::isParentOf( *this, *before_position ) );
            //< позиция before_position должна принадлежать узлу this или быть end
        assert( from_position != from_node.cend() && ::Hierarchy::isParentOf( from_node, *from_position ) );
            //< позиция from_position должна принадлежать узлу from_node и не быть end
        assert( !::Hierarchy::areIdentical( *from_position, *before_position ) );
            // before_position не должна быть from_position
        assert( !::Hierarchy::isAncestorOf( *from_position, *before_position ) );
            // before_position не должна быть частью from_position

        (*from_position).m_parent = this;

        m_nodes.splice( before_position, from_node.m_nodes, from_position );
            // Complexity - Constant.
    }

    template <
        typename _Value,
        template < typename > class _Allocator >
    void Node< _Value, _Allocator >::transfer ( ConstIterator before_position, NodeReference from_node, ConstIterator from_first, ConstIterator from_last ) noexcept
    {
        assert( before_position == cend() || ::Hierarchy::isParentOf( *this, *before_position ) );
            //< позиция before_position должна принадлежать узлу this или быть end
            //< позиция from_last должна принадлежать узлу from_node или быть end
        assert( from_first != from_node.cend() && ::Hierarchy::isParentOf( from_node, *from_first ) );
            //< позиция from_first должна принадлежать узлу from_node и не быть end
        assert( from_last == from_node.cend() || ::Hierarchy::isParentOf( from_node, *from_last ) );
            //< позиция from_last должна принадлежать узлу from_node или быть end

#ifndef NDEBUG
        if ( ::Hierarchy::areIdentical( from_node, *this ) )
            // проверяем, что before_position не входит в диапазон from_position .. end
        {
            for ( ConstIterator iter = from_first; iter != from_last; ++iter )
                assert( !::Hierarchy::areIdentical( *iter, *before_position ) );
                    //< before_position не должен быть в диапазоне [from_position, end).
        }
        else if ( ::Hierarchy::isAncestorOf( from_node, *this ) )
            // проверяем, что this не является частью узлов из интервала from_position .. end
        {
            for ( ConstIterator iter = from_first; iter != from_last; ++iter )
                assert( !::Hierarchy::isAncestorOf( *iter, *before_position ) );
                    //< before_position не должен быть в диапазоне [from_position, end).
        }
#endif
        for ( ConstIterator iter = from_first; iter != from_last; ++iter )
            iter->m_parent = this;

        m_nodes.splice( before_position, from_node.m_nodes, from_first, from_last );
            // Complexity - Up to linear in the number of elements transferred.
    }
}

#endif
