#include <Hierarchy/Node.h>

#ifndef maybe_unused
#define maybe_unused void
#endif

void exampleForParentChild ()
{
    using Node = ::Hierarchy::Node< ::std::string >;
        // Пусть узел иерархии содержит текстовое значение.

    // Создание узлов

    Node first;
    first.value() = "first";
        // Узел инициируется пустым текстом, затем текст задается.

    Node second = "second";
    Node third = "third";
    Node fourth = "fourth";
        // Узлы инициируются текстовыми значениями.

    // Добавление узлов

    /*
     *  NOTE:
     *  Непосредственно между экземплярами значений first, second, third, fourth
     *  нельзя организовать иерархическую связь, так как их время жизни определено
     *  данной областью видимости. Другими словами, в данном случае нельзя передать
     *  владение экземпляром значения созданного на стеке. Можно передавать владение
     *  только от одного узла другому.
     *
     *  Поэтому, добавление одного узла в другой происходит либо с помощью копирования,
     *  либо с помощью перемещения содержимого узлов.
     */

    Node::Iterator second_in_first = first.append( second );
    Node::Iterator third_in_second = second_in_first->append( third );
    Node::Iterator fourth_in_third = third_in_second->append( fourth );
    (maybe_unused) fourth_in_third;
        // Копируем содержимое экземпляров second в first, third в second, fourth в third.
        // Экземпляры second, third, fourth при этом остаются валидными.

            //  first
            //      second
            //          third
            //              fourth

    Node::Iterator third_in_first = first.append( ::std::move( third ) );
    Node::Iterator fourth_in_first = first.append( ::std::move( fourth ) );
        // Перемещаем содержимое экземпляров third и fourth в first.
        // Экземпляры third, fourth теперь находятся в неопределенном состоянии.

            //  first
            //      second
            //          third
            //              fourth
            //      third
            //      fourth

    // Проверка отношений

    assert( true == ::Hierarchy::areIdentical( first, first ) );                        // true
    assert( false == ::Hierarchy::areIdentical( first, second ) );                      // false
    assert( true == ::Hierarchy::areIdentical( *second_in_first, *second_in_first ) );  // true
    assert( false == ::Hierarchy::areIdentical( *second_in_first, *third_in_second ) ); // false
    assert( false == ::Hierarchy::areSibling( third, fourth ) );                        // false
    assert( true == ::Hierarchy::areSibling( *third_in_first, *fourth_in_first ) );     // true
    assert( false == ::Hierarchy::isParentOf( first, second ) );                        // false
    assert( true == ::Hierarchy::isParentOf( first, *second_in_first ) );               // true
    assert( false == ::Hierarchy::isAncestorOf( first, second ) );                      // false
    assert( true == ::Hierarchy::isAncestorOf( first, *fourth_in_third ) );             // true

    // Перебор дочерних узлов

    for ( auto & node : first )
    {
        (maybe_unused) node.value();
            // доступ к значению (ассоциация с ресурсом и любые дополнительные атрибуты)
        (maybe_unused) node.count();
            // получаем количество узлов следующего уровня вложенности

        for ( auto & node1 : node )
            // пошли глубже
        {
            (maybe_unused) node1.value();
            // ...
        }
    }

    // Перенос дочерних узлов

    second.transfer( first );
        // Перемещаем все дочерние узлы из экземпляра first в second.

            //  second
            //      second
            //          third
            //              fourth
            //      third
            //      fourth

    first.transfer( second, second.cbegin() );
        // Перемещаем первый дочерний узел из экземпляра second в first

            //  first
            //      second
            //          third
            //              fourth

            //  second
            //      third
            //      fourth


    first.transfer( first.cend(), second, second.cbegin(), second.cend() );
        // Перемещаем остальные дочерние узлы из экземпляра second в first
        // с указанием позиции в first и диапазона в second.

            //  first
            //      second
            //          third
            //              fourth
            //      third
            //      fourth

    // Копирование/перемещение/извлечение содержимого узлов

    Node first_copied = first;
        // Копируем содержимое экземпляра first в first_copied
        // Копирование происходит и для всех дочерних узлов.
    Node first_moved = ::std::move( first_copied );
        // Перемещаем содержимое экземпляра first_copied в first_moved.
        // При необходимости first_copied извлекается из родителя.
        // Сложность операции O(N), где N - количество узлов родителя.
        // После перемещения экземпляр значения first_copied находится в неопределенном состоянии.

    Node second_from_first = first.take( second_in_first );
    (maybe_unused) second_from_first;
        // Извлечение содержимого узла по его элементу
        // Сложность операции O(1).

            //  second
            //      third
            //          fourth

            //  first
            //      third
            //      fourth

    Node third_from_first = first.takeFirst();
    (maybe_unused) third_from_first;
        // Извлечение содержимого первого дочернего узла

    Node fourth_from_first = first.takeLast();
    (maybe_unused) fourth_from_first;
        // Извлечение содержимого последнего дочернего узла
}

